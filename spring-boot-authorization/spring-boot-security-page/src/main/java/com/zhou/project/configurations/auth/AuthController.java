package com.zhou.project.configurations.auth;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-07 10:58
 * @description: TODO
 */
@Controller
@RequestMapping("auth")
public class AuthController {
    @RequestMapping("login")
    public String login()
    {
        return "login";
    }

    @RequestMapping("index")
    public String index()
    {
        return "index";
    }
}
