package com.zhou.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-07 10:42
 * @description: TODO
 */
@SpringBootApplication
public class SecurityPageApplication {
    public static void main(String[] args) {
        SpringApplication.run(SecurityPageApplication.class, args);
    }
}
