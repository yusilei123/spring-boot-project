package com.zhou.project.moduels.user.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-07 16:12
 * @description: TODO
 */
@Controller
@RequestMapping("user")
public class UserController {

    @RequiresPermissions("user:insert")
    @RequestMapping("insert")
    public String insert()
    {
        return "user/insert";
    }

}
