package com.zhou.project.moduels.user.entity;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-08 15:47
 * @description: TODO
 */
@Data
public class User {
    private int id;
    // 登录账号
    private String name;
    // 登录密码
    private String pass;
    // 用户昵称
    private String nick;
    // 加密盐值
    private String salt;
    //用户所有角色值，用于shiro做角色权限的判断
    private Set<String> roles = new HashSet<>();
    //用户所有权限值，用于shiro做资源权限的判断
    private Set<String> perms = new HashSet<>();
}
