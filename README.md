# spring-boot-project

#### 介绍
基于SpirngBoot3.0 M2的与各个框架技术整合, 项目持续更新中。后续涉及完整项目配置等信息。。。

#### 软件架构
软件架构说明


#### 安装教程

1.  下载项目到本地
2.  导入DEA引入相关依赖
3.  根据自己需要查看相关项目

#### 使用说明

1.  spring-boot-data 数据库相关
    1.1.    spring-boot-jpa             springboot整合hibernate
    1.2.    spring-boot-mybatis         springboot整合mybatis
    1.3.    spring-boot-mybatis-plus    springboot整合mybatis-plsu
2.  spring-boot-docs Restful API 文档
    2.1.    spring-boot-smart-doc            springboot整合smart doc
    2.2.    spring-boot-spring--restdocs     springboot整合spring Restdocs
    2.3.    spring-boot-swagger              springboot整合swagger(swagger版本为升级暂不支持springboot3)
3.  spring-boot-web  springboot整合页面
    3.1.    spring-boot-freemarker    springobot整合freemarker
    3.2.    spring-boot-thymeleaf     springboot整合thymeleaf
