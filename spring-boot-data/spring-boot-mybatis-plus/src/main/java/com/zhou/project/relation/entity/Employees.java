package com.zhou.project.relation.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:12
 * @description: [员工实体对象]
 */
@Getter
@Setter
@ToString
@TableName(value = "employees", resultMap = "employeesResultMap")
public class Employees {
    /**
     * 员工主键
     */
    @TableId(type = IdType.AUTO)
    private int employeesId;
    /**
     * 员工名称
     */
    @TableField
    private String employeesName;
    /**
     * 员工岗位
     */
    @TableField
    private String employeesJobs;
    /**
     * 员工薪资
     */
    @TableField
    private String employeesSalary;
    /**
     * 员工所属部门
     */
    @TableField(exist = false)
    private Department department;
    /**
     * 插入时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Timestamp createdTime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private Timestamp updatedTime;
}
