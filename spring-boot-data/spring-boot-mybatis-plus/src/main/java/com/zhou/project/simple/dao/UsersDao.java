package com.zhou.project.simple.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhou.project.simple.entity.Users;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-26 16:45
 * @description: [用户数据层接口]
 */
public interface UsersDao extends BaseMapper<Users> {

}
