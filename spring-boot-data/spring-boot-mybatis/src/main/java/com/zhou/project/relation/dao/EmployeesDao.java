package com.zhou.project.relation.dao;

import com.zhou.project.relation.entity.Employees;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:18
 * @description: [员工数据层接口]
 */
public interface EmployeesDao {
    int insert(Employees entity);
    Employees load(int id);
}
