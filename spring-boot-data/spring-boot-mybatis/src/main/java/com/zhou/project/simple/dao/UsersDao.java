package com.zhou.project.simple.dao;

import com.zhou.project.simple.entity.Users;

import java.util.List;
import java.util.Map;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-26 16:45
 * @description: [用户数据层接口]
 */
public interface UsersDao {
    /**
     * 插入用户信息
     * @param entity 用户对象
     * @return 影响条数
     */
    int insert(Users entity);
    /**
     * 修改用户信息
     * @param entity 用户对象
     * @return 影响条数
     */
    int update(Users entity);
    /**
     * 删除用户信息
     * @param id 用户主键
     * @return 影响条数
     */
    int delete(int id);
    /**
     * 删除用户组
     * @param ids 用户主键组
     * @return 影响条数
     */
    int deletes(int... ids);
    /**
     * 主键查询用户
     * @param id 用户主键
     * @return 用户对象
     */
    Users queryId(int id);
    /**
     * 条件查询总数
     * @param params 动态条件
     * @return 总条数
     */
    long queryCount(Map<String, Object> params);
    /**
     * 条件查询集合
     * @param params 动态条件
     * @return 结果集
     */
    List<Users> queryPage(Map<String, Object> params);
}
