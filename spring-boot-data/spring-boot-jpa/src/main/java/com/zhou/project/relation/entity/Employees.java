package com.zhou.project.relation.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:12
 * @description: [员工实体对象]
 */
@Getter
@Setter
@Entity
@Table(name = "employees")
@EntityListeners(value = AuditingEntityListener.class)
public class Employees {
    /**
     * 员工主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int employeesId;
    /**
     * 员工名称
     */
    @Column(name = "employees_name", nullable = false)
    private String employeesName;
    /**
     * 员工岗位
     */
    @Column(name = "employees_jobs")
    private String employeesJobs;
    /**
     * 员工薪资
     */
    @Column(name = "employees_salary")
    private String employeesSalary;
    /**
     * 添加日期 自动更新
     */
    @CreatedDate
    @Column(name = "created_time", nullable = false, updatable = false)
    private Timestamp createdTime;
    /**
     * 修改日期 自动更新
     */
    @LastModifiedDate
    @Column(name = "updated_time")
    private Timestamp updatedTime;
    /**
     * 员工所属部门
     */
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "department_id")
    private Department department;
}
