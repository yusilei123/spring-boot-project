package com.zhou.project.relation.service;

import com.zhou.project.relation.dao.DepartmentDao;
import com.zhou.project.relation.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:19
 * @description: TODO
 */
@Service
@Transactional(readOnly = true)
public class DepartmentService {

    @Autowired
    private DepartmentDao departmentDao;

    /**
     * 保证逻辑层同一事务单元 ACID 原子性
     * @param entity 员工对象(包含部门)
     */
    @Transactional
    public void save(Department entity)
    {
        //首先保存部门  @OneToMany(cascade = CascadeType.ALL, targetEntity = Employees.class)
        //获取部门主键插入员工表 部门外键  @JoinColumn(name = "department_id")
        departmentDao.save(entity);
    }

    /**
     * 主键查询部门对象
     * @param id 部门主键
     * @return 部门对象
     */
    public Department load(int id)
    {
        return departmentDao.findById(id).get();
    }

}
