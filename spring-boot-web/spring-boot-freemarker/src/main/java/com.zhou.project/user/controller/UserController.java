package com.zhou.project.user.controller;

import com.zhou.project.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-02 16:47
 * @description: TODO
 */
@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 模拟分页查询
     * @param current   当前页
     * @param sizes     每页条数
     * @param modelMap  封装结果集
     * @return  跳转至用户展示页面
     */
    @RequestMapping(value = "query")
    public String query(
        @RequestParam(required = false, defaultValue = "1") int current,
        @RequestParam(required = false, defaultValue = "10") int sizes,
        ModelMap modelMap)
    {
        modelMap.put("current", current);
        modelMap.put("list", userService.page(current, sizes));
        return "user/list";
    }

}
